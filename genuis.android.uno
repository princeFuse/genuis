using Uno;
using Uno.Collections;
using Fuse;
using Fuse.Platform;
using Fuse.Scripting;
using Uno.Permissions;

using Uno.Compiler.ExportTargetInterop;

[ForeignInclude(Language.Java, "com.thegrizzlylabs.geniusscan.sdk.core.GeniusScanLibrary")]
[ForeignInclude(Language.Java, "com.thegrizzlylabs.geniusscan.sdk.pdf.PDFDocument")]
[ForeignInclude(Language.Java, "com.thegrizzlylabs.geniusscan.sdk.pdf.PDFGeneratorError")]
[ForeignInclude(Language.Java, "com.thegrizzlylabs.geniusscan.sdk.pdf.PDFPage")]
[ForeignInclude(Language.Java, "com.thegrizzlylabs.geniusscan.sdk.pdf.PDFSize")]
[ForeignInclude(Language.Java, "com.thegrizzlylabs.geniusscan.sdk.camera.CameraManager")]
[ForeignInclude(Language.Java, "com.thegrizzlylabs.geniusscan.sdk.core.RotationAngle")]
[ForeignInclude(Language.Java, "com.thegrizzlylabs.geniusscan.sdk.core.ScanContainer")]
[ForeignInclude(Language.Java, "com.thegrizzlylabs.geniusscan.sdk.camera.ScanFragment")]
[ForeignInclude(Language.Java, "com.thegrizzlylabs.geniusscan.sdk.core.Scan")]
[ForeignInclude(Language.Java, "com.thegrizzlylabs.geniusscan.sdk.core.ImageType")]
[ForeignInclude(Language.Java, "com.thegrizzlylabs.geniusscan.sdk.core.ProcessingParameters")]
[ForeignInclude(Language.Java, "com.thegrizzlylabs.geniusscan.sdk.core.Quadrangle")]
[ForeignInclude(Language.Java, "com.thegrizzlylabs.geniusscan.sdk.camera.PreviewSurfaceView")]
[ForeignInclude(Language.Java, "android.hardware.Camera")]
[ForeignInclude(Language.Java, "android.os.Handler")]
[ForeignInclude(Language.Java, "android.content.Intent")]
[ForeignInclude(Language.Java, "android.view.WindowManager")]
[ForeignInclude(Language.Java, "android.view.SurfaceHolder.Callback")]
[ForeignInclude(Language.Java, "android.view.View", "android.content.Context")]
[ForeignInclude(Language.Java, "android.os.AsyncTask")]
[ForeignInclude(Language.Java, "android.app.ProgressDialog")]
[Require("AndroidManifest.Permission", "android.permission.CAMERA")]
[Require("AndroidManifest.Permission", "android.permission.WRITE_EXTERNAL_STORAGE")]
[Require("AndroidManifest.Permission", "android.hardware.camera")]
[Require("AndroidManifest.Permission", "android.hardware.screen.portrait")]
[ForeignInclude(Language.Java, "com.fuse.Activity")]
[ForeignInclude(Language.Java, "android.provider.MediaStore", "android.content.Context")]
[ForeignInclude(Language.Java, "java.io.File")]
[ForeignInclude(Language.Java, "java.util.ArrayList")]


public class Genuis: NativeModule
{

  public Genuis()
  {
    AddMember(new NativeFunction("Init",
  (NativeCallback)Init));


  AddMember(new NativeFunction("LaunchScan",
(NativeCallback)LaunchScan));

  }

  object Init(Context c, object[] args)
  {
    Permissions.Request(Permissions.Android.CAMERA);
    Fuse.UpdateManager.PostAction(Init);

    return null;
  }



  object LaunchScan(Context c, object[] args)
  {

      LaunchScan();


    return null;
  }






[Foreign(Language.Java)]
  static extern(Android) void Init()
  @{
    GeniusScanLibrary.init(Activity.getRootActivity(), "533c50045d500403035604593957474d1402400f5b465d551b1850145e4e5542116b55560103020453040602");
  @}


  static extern(!Android) void Init()
  {
    debug_log("Genuis not supported on this platform.");
  }

  [Foreign(Language.Java)]
  public extern(Android) void LaunchScan()
  @{


        Activity.getRootActivity().runOnUiThread(new Runnable() {

          CameraManager cameraManager = new CameraManager(Activity.getRootActivity(), new CameraManager.Callback() {
           @Override
           public void onCameraReady(CameraManager cameraManager, Camera camera) {}

           @Override
           public void onCameraFailure(CameraManager cameraManager) {}

           @Override
           public void onShutterTriggered(CameraManager cameraManager) {}

           @Override
           public void onPictureTaken(CameraManager cameraManager, int cameraOrientation, ScanContainer scanContainer) {}
           });

          PreviewSurfaceView previewSurfaceView = new PreviewSurfaceView(Activity.getRootActivity()) {
             @Override
             public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {}

             @Override
             public void setAspectRatio(int width, int height) {}
         };

         Camera.PreviewCallback previewCallback = new Camera.PreviewCallback(){
               @Override
               public void onPreviewFrame(byte[] data, Camera camera) {}
           };

         @Override
         public void run() {
             cameraManager.startPreview(previewSurfaceView,  previewCallback);
         }
        });

  @}


      static extern(!Android) void  LaunchScan()
      {
        debug_log("Genuis not supported on this platform.");
      }



      // [Foreign(Language.Java)]
      // static extern(Android) void TakePicture()
      // @{
      //
      // @}
      //
      //
      // static extern(!Android) void TakePicture()
      // {
      //
      // }
      //
      //
      // [Foreign(Language.Java)]
      // static extern(Android) void StopCamera()
      // @{
      //
      // @}
      //
      //
      // static extern(!Android) void StopCamera()
      // {
      //
      // }


      // [Foreign(Language.Java)]
      // static extern(Android) void Init()
      // @{
      //
      // @}
      //
      //
      // static extern(!Android) void Init()
      // {
      //
      // }


}
